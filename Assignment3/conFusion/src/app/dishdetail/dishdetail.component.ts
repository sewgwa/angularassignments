import { Component, OnInit, Input} from '@angular/core';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Comment } from '../shared/comment';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';

import 'rxjs/add/operator/switchmap';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {

	date: Date;
	dish: Dish;
	dishIds: number[];
	prev: number;
	next: number;
	ratingForm: FormGroup;
	newComment: Comment = {
		'author': '',
		'rating': 5,
		'comment': '',
		'date': ''
	}

	formErrors = {
		'author': '',
		'comment': '',
	};

	validationMessages = {
		'author': {
		  'required': 'Author is required.',
		  'minlength': 'Author name must be at leasts 2 characters long'
		},
		'comment': {
		  'required': 'Comment is required.'
		}
	};

	constructor(private dishservice: DishService,
		private route: ActivatedRoute,
		private location: Location,
		private fb: FormBuilder) { 
		this.createForm();
	}

	ngOnInit() {
		this.dishservice.getDishIds()
			.subscribe(dishIds => this.dishIds = dishIds);

		this.route.params
			.switchMap((params: Params) => this.dishservice.getDish(+params['id']))
			.subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id) });

	}

	setPrevNext(dishId: number) {
		let index = this.dishIds.indexOf(dishId);
		this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
		this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
	}

	goBack(): void {
		this.location.back();
	}

	onValueChanged(data?: any) {
		if (!this.ratingForm) {
			return;
		}
		const form = this.ratingForm; 

		for (const field in this.formErrors) {
			this.formErrors[field] = '';
			const control = form.get(field);
			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
			  		this.formErrors[field] += messages[key]+' ';
				}
			}
		}
	}

	createForm() {
	    this.ratingForm = this.fb.group({
	      author: ['', [Validators.required, Validators.minLength(2)] ],
	      comment: ['', [Validators.required] ]
	    });

	    this.ratingForm.valueChanges.subscribe(data => this.onValueChanged(data));

	    this.onValueChanged(); // (re)set form validation messages
    }

    onSubmit() {
    	this.date = new Date();
    	this.newComment.date = this.date.toISOString();
    	this.dish.comments.push({
    		'author': this.newComment.author,
    		'rating': this.newComment.rating,
    		'comment': this.newComment.comment,
    		'date': this.newComment.date
    	});
    	this.ratingForm.reset({
    		'author': '',
    		'rating': 5,
    		'commenet': '',
    		'date': ''
    	});
    }

}
