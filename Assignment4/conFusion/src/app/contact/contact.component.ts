import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { flyInOut, visibility, expand } from '../animations/app.animation';
import { FeedbackService } from '../services/feedback.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

	feedbackForm: FormGroup;
	feedback: Feedback;
	contactType = ContactType;
  visibility = 'shown';
  confirmVisibility = 'hidden';
  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };
  confirmation: Feedback = {
    firstname: '',
    lastname: '',
    telnum: 0,
    email: '',
    agree: false,
    contacttype: '',
    message: ''
  };
  submittingForm = false;

  validationMessages = {
    'firstname': {
      'required': 'First name is required.',
      'minlength': 'First name must be at leasts 2 characters long',
      'maxlength': 'First name cannot be more than 25 characters long',
    },
    'lastname': {
      'required': 'Last name is required.',
      'minlength': 'Last name must be at leasts 2 characters long',
      'maxlength': 'Last name cannot be more than 25 characters long',
    },
    'telnum': {
      'required': 'Tel. number is required.',
      'pattern': 'Tel. number must contain only numbers.',
    },
    'email': {
      'required': 'Email is required.',
      'email': 'Email is not in valid form.'
    }
  };


	constructor(private fb: FormBuilder, 
    private feedbackservice: FeedbackService,
    @Inject('BaseURL') private BaseURL) { 
		this.createForm();
	}

	ngOnInit() {
	}

	createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      telnum: ['', [Validators.required, Validators.pattern] ],
      email: ['', [Validators.required, Validators.email] ],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) {
      return;
    }
    const form = this.feedbackForm; 

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key]+' ';
        }
      }
    }
  }

  onSubmit() {
    this.feedback = this.feedbackForm.value;
    this.visibility = 'hidden';
    this.submittingForm = true;
    this.feedbackservice.submitFeedback(this.feedback)
        .subscribe(confirmation => { 
          console.log(confirmation);
          this.confirmation = confirmation;
          this.submittingForm = false;
          this.confirmVisibility = 'shown';
          setTimeout(() => { this.confirmVisibility = 'hidden'; this.visibility = 'shown' }, 5000);
          this.feedbackForm.reset({
            firstname: '',
            lastname: '',
            telnum: '',
            email: '',
            agree: false,
            contacttype: 'None',
            message: ''
          });
        });
  }


}
